package pl.manukodu.androidbondingpoc

import android.Manifest.permission.*
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.github.kayvannj.permission_utils.Func
import com.github.kayvannj.permission_utils.PermissionUtil
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val permissionsRequestCode = 6745
    private val allPermissionRequest = PermissionUtil.with(this).request(ACCESS_COARSE_LOCATION, BLUETOOTH, BLUETOOTH_ADMIN)
    private val onPermissionGranted = object : Func() {
        override fun call() {
            if (bluetoothAdapter.isEnabled) {
                performScanning()
            } else {
                val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBluetoothIntent, bluetoothRequestCode)
            }
        }
    }
    private val onAnyPermissionDenied = object : Func() {
        override fun call() {
            showToast("All permissions [ACCESS_COARSE_LOCATION, BLUETOOTH, " +
                    "BLUETOOTH_ADMIN] need to be granted in order to scan and bond to BLE devices",
                    false)
            finish()
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        allPermissionRequest.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private lateinit var bluetoothManager: BluetoothManager
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private val scanner = bluetoothAdapter.bluetoothLeScanner
    private val bluetoothRequestCode = 354
    private val bondFilter = IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
    private val bondingReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == BluetoothDevice.ACTION_BOND_STATE_CHANGED) {
                listAdapter.notifyDataSetChanged()
            }
        }
    }
    private var scanHandler = Handler()
    private fun performScanning() {
        refresh.isRefreshing = true
        val scanCallback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                super.onScanResult(callbackType, result)
                listAdapter.add(result.device)
            }
        }
        scanner.startScan(scanCallback)
        scanHandler.postDelayed({
            scanner.stopScan(scanCallback)
            refresh.isRefreshing = false
        }, 5000)
    }

    private lateinit var listAdapter: DeviceListAdapter
    private val deviceListListener = object : DeviceListAdapter.DeviceListAdapterListener {
        override fun onBondClicked(device: BluetoothDevice) {
            when (device.bondState) {
                BluetoothDevice.BOND_NONE ->
                    if (!device.createBond()) {
                        showToast("Unable to bond $device", true)
                    }
                BluetoothDevice.BOND_BONDED -> {
                    device.removeBond()
                }
            }
        }

        override fun onReadClicked(device: BluetoothDevice) {
            gatt = device.connectGatt(this@MainActivity, false, gattCallback)
        }
    }

    private lateinit var gatt : BluetoothGatt
    private val listHandler = Handler()
    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            listHandler.post { listAdapter.notifyDataSetChanged() }
            if (newState == BluetoothProfile.STATE_CONNECTED) gatt.discoverServices()
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(gatt, status)
            val uuids = gatt.services.map {
                val serviceUUID = it.uuid.toString().split("-").first()
                val characteristicUUIDs = it.characteristics.map { it.uuid.toString().split("-").first() }
                "SER: $serviceUUID, CHAR: $characteristicUUIDs"
            }
            showAlert("Discovered services", uuids.toString())
            gatt.disconnect()
        }
    }

    private val alertHandler = Handler()
    private fun showAlert(title: String, message: String) {
        alertHandler.post {
            val alertDialog = AlertDialog.Builder(this@MainActivity).create()
            alertDialog.setTitle(title)
            alertDialog.setMessage(message)
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
        }
    }

    private val onRefreshListener = SwipeRefreshLayout.OnRefreshListener {
        listAdapter.removeAll()
        performScanning()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bluetoothManager = this@MainActivity.getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        listAdapter = DeviceListAdapter(this, bluetoothManager)
        listAdapter.clickListener = deviceListListener

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = listAdapter
        list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL))
        refresh.setOnRefreshListener(onRefreshListener)
        registerReceiver(bondingReceiver, bondFilter)

        allPermissionRequest.onAllGranted(onPermissionGranted)
        allPermissionRequest.onAnyDenied(onAnyPermissionDenied)
        allPermissionRequest.ask(permissionsRequestCode)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == bluetoothRequestCode) {
            if (resultCode == 0) {
                showToast("Bluetooth disabled", false)
                finish()
            } else performScanning()
        }
    }
    private val toastHandler = Handler()
    private fun showToast(message: String, short: Boolean) {
        toastHandler.post {
            Toast.makeText(this@MainActivity, message, if (short) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
        }
    }
}

fun BluetoothDevice.removeBond() {
    this.javaClass.getMethod("removeBond")?.invoke(this)
}
