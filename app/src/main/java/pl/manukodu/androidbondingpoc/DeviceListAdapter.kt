package pl.manukodu.androidbondingpoc

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.list_layout.view.*

class DeviceListAdapter(private var context: Context, private val bluetoothManager: BluetoothManager) : RecyclerView.Adapter<DeviceListViewHolder>() {

    var clickListener: DeviceListAdapterListener? = null

    interface DeviceListAdapterListener {
        fun onBondClicked(device: BluetoothDevice)
        fun onReadClicked(device: BluetoothDevice)
    }

    private var devices = mutableListOf<BluetoothDevice>()

    fun add(device: BluetoothDevice) {
        if (!devices.contains(device)) {
            devices.add(device)
            notifyItemInserted(devices.indexOf(device))
        }
    }

    fun removeAll() {
        devices.removeAll(devices)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DeviceListViewHolder? {
        val itemView = LayoutInflater.from(context).inflate(R.layout.list_layout, parent, false)
        return DeviceListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DeviceListViewHolder, position: Int) {
        val device = devices[position]
        holder.bind(device, bluetoothManager)
        holder.itemView.buttonBond.setOnClickListener {
            clickListener?.onBondClicked(device)
        }
        holder.itemView.buttonRead.setOnClickListener {
            clickListener?.onReadClicked(device)
        }
    }

    override fun getItemCount(): Int {
        return devices.size
    }
}
