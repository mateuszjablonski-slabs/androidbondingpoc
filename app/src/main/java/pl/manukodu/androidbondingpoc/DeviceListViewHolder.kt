package pl.manukodu.androidbondingpoc

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothProfile
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class DeviceListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val deviceNameText: TextView = itemView.findViewById(R.id.textView)
    private val buttonBond: TextView = itemView.findViewById(R.id.buttonBond)
    private val buttonRead: TextView = itemView.findViewById(R.id.buttonRead)

    fun bind(device: BluetoothDevice, bluetoothManager: BluetoothManager) {
        val text = "${device.name} (${device.address})"
        deviceNameText.text = text
        when(device.bondState) {
            BluetoothDevice.BOND_NONE -> {
                buttonBond.text = "Bond"
                buttonBond.setTextColor(Color.BLACK)
            }
            BluetoothDevice.BOND_BONDING -> {
                buttonBond.text = "Bonding..."
                buttonBond.setTextColor(Color.RED)
            }
            BluetoothDevice.BOND_BONDED -> {
                buttonBond.text = "Bonded"
                buttonBond.setTextColor(Color.BLUE)
            }
        }
        val state = bluetoothManager.getConnectionState(device, BluetoothProfile.GATT)
        buttonRead.setTextColor(if (state == BluetoothProfile.STATE_DISCONNECTED) Color.BLACK else Color.RED)
        when (state) {
            BluetoothProfile.STATE_DISCONNECTED -> buttonRead.text = "Read"
            BluetoothProfile.STATE_CONNECTING-> buttonRead.text = "Connecting..."
            BluetoothProfile.STATE_CONNECTED -> buttonRead.text = "Reading..."
            BluetoothProfile.STATE_DISCONNECTING-> buttonRead.text = "Disconnecting..."
        }
    }
}